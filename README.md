# GSR Helper Scripts

This project holds helper scripts for GSR.

# Commands to initialize OpenLDAP (`slapd`)

```shell
# prepare openldap server directory
/bin/bash -c "$(curl -fsSL https://gitlab.com/ipb-gsr/helper-scripts/-/raw/main/slapd-init/prepare.sh)"
# check the directory was initialized with 2 'student' users and 1 'student' group
ldapsearch -LLL -x -b 'ou=users,dc=ipb,dc=pt' dn uidNumber memberUid
```

Output should be something like:
```ldif
dn: ou=users,dc=ipb,dc=pt

dn: cn=students,ou=users,dc=ipb,dc=pt
memberUid: 2001
memberUid: 2002

dn: cn=student01,ou=users,dc=ipb,dc=pt
uidNumber: 2001

dn: cn=student02,ou=users,dc=ipb,dc=pt
uidNumber: 2002
```